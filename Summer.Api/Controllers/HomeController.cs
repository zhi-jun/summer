﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Summer.Api.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public async Task Index()
        {
            await Response.WriteAsync(DateTime.Now.ToLocalTime().ToString());
        }

        [HttpGet]
        public async Task Error()
        {
            throw new Exception("测试异常");
        }
    }
}
