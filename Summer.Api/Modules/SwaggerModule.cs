﻿using Microsoft.AspNetCore.Builder;
using Microsoft.DotNet.PlatformAbstractions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Summer.Core.Module;
using System;
using System.IO;

namespace Summer.Api.Modules
{
    /// <summary>
    /// Swagger模块
    /// </summary>
    public class SwaggerModule : IAppModule
    {
        public ModuleLevel Level => ModuleLevel.Framework;

        public int Order => 1;

        /// <summary>
        /// 将模块服务添加到依赖注入服务容器中
        /// </summary>
        /// <param name="services">依赖注入服务容器</param>
        /// <returns></returns>
        public IServiceCollection AddServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "API",
                    Description = $"Summer Api  " +
                    $"<br/>build time: { File.GetLastWriteTime(GetType().Assembly.Location).ToString("yyyy-MM-dd HH:mm") } " +
                    $"<br/>run time: { DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd HH:mm") }"
                });
                var xmlPath = Path.Combine(ApplicationEnvironment.ApplicationBasePath, "Summer.Api.xml");
                c.IncludeXmlComments(xmlPath, true);
            });

            return services;
        }

        /// <summary>
        /// 初始化 模块
        /// </summary>
        /// <param name="provider"></param>
        public void InitModule(IApplicationBuilder provider)
        {
            provider.UseSwagger();
            provider.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("swagger/v1/swagger.json", "API v1");
                c.RoutePrefix = string.Empty;
            });
        }
    }
}
