﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Summer.AspNetCore.Middleware;
using Summer.Core.Module;

namespace Summer.Api.Modules
{
    /// <summary>
    /// Mvc模块
    /// </summary>
    public class MvcModule : IAppModule
    {
        /// <summary>
        /// 等级
        /// </summary>
        public ModuleLevel Level => ModuleLevel.Core;

        /// <summary>
        /// 排序
        /// </summary>
        public int Order => 1;

        /// <summary>
        /// 将模块服务添加到依赖注入服务容器中
        /// </summary>
        /// <param name="services">依赖注入服务容器</param>
        /// <returns></returns>
        public IServiceCollection AddServices(IServiceCollection services)
        {
            services
                .AddRouting()
                .AddControllers();
            return services;
        }

        /// <summary>
        /// 初始化 模块
        /// </summary>
        /// <param name="provider"></param>
        public void InitModule(IApplicationBuilder provider)
        {
            provider
                .UseMiddleware<CostomErrorMiddleware>()
                .UseRouting()
                .UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
