using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using NLog.Extensions.Logging;

namespace Summer.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Host.CreateDefaultBuilder(args)
              .ConfigureWebHostDefaults(builder =>
              {
                  builder.UseStartup<Startup>()
                  .ConfigureLogging(logging => logging.AddNLog()); // ע��nlog
              })
              .Build()
              .Run();
        }
    }
}
