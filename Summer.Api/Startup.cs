using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Summer.Core.Dependency;

namespace Summer.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // 注入容器
            services.AddModuleManage();
        }

        public void Configure(IApplicationBuilder app)
        {
            // 处理管道
            app.UseModuleManage();
        }
    }
}
