﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Summer.Core.Cache;
using Summer.Core.Dependency;
using System.Threading.Tasks;

namespace Summer.AspNetCore.Attributes
{
    /// <summary>
    /// 基于特性拦截的缓存方案
    /// 缓存对象: Controller/Action
    /// 缓存Key: controller + action + querystring
    /// 缓存时长: 配置文件 appsettings => CacheExpiresIn
    /// </summary>
    public class CacheFilterAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// 缓存时长(分钟)
        /// </summary>
        private int expiresIn
        {
            get
            {
                var configuration = ServiceLocator.Instance.GetService<IConfiguration>();
                return configuration.GetValue<int>("CacheExpiresIn");
            }
        }

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            //不缓存
            if (expiresIn <= 0)
                return;
            //当前方法
            ControllerActionDescriptor actionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            //获取自定义缓存键
            string cacheKey = $"{actionDescriptor.ControllerName}:{actionDescriptor.ActionName}:{context.HttpContext.Request.QueryString}";
            //根据key获取相应的缓存值
            IActionResult result = MemoryCacheHelper.Get<IActionResult>(cacheKey);
            if (result == null)
            {
                //执行方法
                result = (await next()).Result;
                //存入缓存
                MemoryCacheHelper.Set(cacheKey, result, expiresIn);
            }
            //结束http处理,返回缓存结果
            context.Result = result;
        }
    }
}
