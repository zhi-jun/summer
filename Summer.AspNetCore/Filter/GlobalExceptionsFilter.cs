﻿//using Microsoft.AspNetCore.Hosting;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Mvc.Filters;
//using Summer.Core.Logger;

//namespace Summer.AspNetCore.Filter
//{
//    /// <summary>
//    /// 全局异常处理
//    /// </summary>
//    public class GlobalExceptionsFilter : IExceptionFilter
//    {
//        private readonly IHostingEnvironment _env;
//        public GlobalExceptionsFilter(IHostingEnvironment env)
//        {
//            _env = env;
//        }


//        public void OnException(ExceptionContext context)
//        {
//            //记录日志
//            LogWriter.Error(
//                $"【异常信息】：${context.Exception.Message} \r\n" +
//                $"【堆栈调用】：${context.Exception.StackTrace}");

//            var json = new JsonErrorResponse();
//            json.Message = context.Exception.Message;//错误信息
//            if (_env.IsDevelopment())
//            {
//                json.DevelopmentMessage = context.Exception.StackTrace;//堆栈信息
//            }
//            //返回错误信息
//            context.Result = new InternalServerErrorObjectResult(json);
//        }
//    }

//    public class InternalServerErrorObjectResult : ObjectResult
//    {
//        public InternalServerErrorObjectResult(object value) : base(value)
//        {
//            StatusCode = StatusCodes.Status500InternalServerError;
//        }
//    }

//    //返回错误信息
//    public class JsonErrorResponse
//    {
//        /// <summary>
//        /// 生产环境的消息
//        /// </summary>
//        public string Message { get; set; }
//        /// <summary>
//        /// 开发环境的消息
//        /// </summary>
//        public string DevelopmentMessage { get; set; }
//    }
//}