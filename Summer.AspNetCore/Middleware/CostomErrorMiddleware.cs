﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Summer.Core.Logger;
using System;
using System.Threading.Tasks;

namespace Summer.AspNetCore.Middleware
{
    /// <summary>
    /// 自定义的错误处理类
    /// </summary>
    public class CostomErrorMiddleware
    {
        private readonly RequestDelegate _next;
        private IHostingEnvironment _env;
        /// <summary>
        /// DI,注入环境变量
        /// </summary>
        /// <param name="next"></param>
        /// <param name="environment"></param>
        public CostomErrorMiddleware(RequestDelegate next, IHostingEnvironment environment)
        {
            this._next = next;
            this._env = environment;
        }
        /// <summary>
        /// 实现Invoke方法
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (Exception ex)
            {
                await HandleError(context, ex);
            }
        }
        /// <summary>
        /// 错误信息处理方法
        /// </summary>
        /// <param name="context"></param>
        /// <param name="ex"></param>
        /// <returns></returns>
        private async Task HandleError(HttpContext context, Exception ex)
        {
            string errorMsg = $"\r\n【异常信息】：{ex.Message} \r\n【堆栈调用】：{ex.StackTrace}";
            //记录日志
            LogWriter.Error(errorMsg);

            //返回错误信息
            context.Response.StatusCode = 500;
            context.Response.ContentType = "text/json;charset=utf-8;";
            if (_env.IsDevelopment())
            {
                await context.Response.WriteAsync(errorMsg);
            }
            else
            {
                await context.Response.WriteAsync("抱歉，服务端出错了");
            }
        }
    }
}
