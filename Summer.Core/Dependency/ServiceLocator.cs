﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Summer.Core.Dependency
{
    /// <summary>
    /// 应用程序服务定位器
    /// </summary>
    public sealed class ServiceLocator : IDisposable
    {
        //Lazy: 1.第一次使用时才实例化 2.线程安全的
        private static readonly Lazy<ServiceLocator> InstanceLazy = new Lazy<ServiceLocator>(() => new ServiceLocator());
        //用于注册服务,获取服务
        private IServiceProvider _provider;

        /// <summary>
        /// 获取 服务器定位器实例
        /// </summary>
        public static ServiceLocator Instance => InstanceLazy.Value;

        /// <summary>
        /// 设置应用程序服务提供者
        /// </summary>
        public void SetServiceProvider(IServiceProvider provider)
        {
            _provider = provider;
        }

        /// <summary>
        /// 解析指定类型的服务实例
        /// </summary>
        public T GetService<T>()
        {
            return _provider.GetService<T>();
        }

        #region Dispose
        public void Dispose()
        {
            _provider = null;
        }
        #endregion
    }
}
