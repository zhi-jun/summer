﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Summer.Core.Module;
using System;

namespace Summer.Core.Dependency
{
    /// <summary>
    /// 依赖注入服务集合扩展
    /// </summary>
    public static class ServiceExtensions
    {
        /// <summary>
        /// 模块注入容器
        /// </summary>
        public static IServiceCollection AddModuleManage(this IServiceCollection services)
        {
            IAppModuleManager manager = services.GetOrAddSingletonInstance<IAppModuleManager>(new AppModuleManager());
            manager.LoadModules(services);
            return services;
        }

        /// <summary>
        /// 处理管道
        /// </summary>
        public static IApplicationBuilder UseModuleManage(this IApplicationBuilder app)
        {
            IServiceProvider provider = app.ApplicationServices;
            IAppModuleManager manager = provider.GetService<IAppModuleManager>();
            manager.UseModules(app);

            return app;
        }

        /// <summary>
        /// 获取指定类型的日志对象
        /// </summary>
        /// <typeparam name="T">非静态强类型</typeparam>
        /// <returns>日志对象</returns>
        public static ILogger<T> GetLogger<T>(this IServiceProvider provider)
        {
            ILoggerFactory factory = provider.GetService<ILoggerFactory>();
            return factory.CreateLogger<T>();
        }
    }
}
