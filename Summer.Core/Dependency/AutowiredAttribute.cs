﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Summer.Core.Dependency
{
    /// <summary>
    /// 依赖注入行为特性
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class AutowiredAttribute : Attribute
    {
        /// <summary>
        /// 初始化一个<see cref="AutowiredAttribute"/>类型的新实例
        /// </summary>
        public AutowiredAttribute(ServiceLifetime lifetime)
        {
            Lifetime = lifetime;
        }

        /// <summary>
        /// 获取 生命周期类型，代替
        /// <see cref="ISingletonDependency"/>,<see cref="IScopeDependency"/>,<see cref="ITransientDependency"/>三个接口的作用
        /// </summary>
        public ServiceLifetime Lifetime { get; }
    }
}
