﻿using Microsoft.Extensions.DependencyModel;
using System.Collections.Generic;
using System.Reflection;

namespace Summer.Core.Common
{
    public class AssemblyFinder
    {
        static Assembly[] assemblys;

        /// <summary>
        /// 所有程序集
        /// </summary>
        public static Assembly[] AllAssemblys
        {
            get
            {
                if (assemblys != null)
                    return assemblys;

                DependencyContext context = DependencyContext.Default;
                List<string> names = new List<string>();
                foreach (CompilationLibrary library in context.CompileLibraries)
                {
                    string name = library.Name;
                    if (!names.Contains(name) && name.StartsWith("Summer.Api"))
                    {
                        names.Add(name);
                    }
                }
                assemblys = LoadFiles(names);


                return assemblys;
            }
        }

        private static Assembly[] LoadFiles(IEnumerable<string> files)
        {
            List<Assembly> assemblies = new List<Assembly>();
            foreach (string file in files)
            {
                AssemblyName name = new AssemblyName(file);

                assemblies.Add(Assembly.Load(name));
            }
            return assemblies.ToArray();
        }
    }
}
