﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Summer.Core.Extentions;
using Summer.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Summer.Core.Dependency;

namespace Summer.Core.Module
{
    public class AppModuleManager : IAppModuleManager
    {

        public AppModuleManager()
        {
            Modules = new List<IAppModule>();
        }

        /// <summary>
        /// 获取 自动检索到的所有模块信息
        /// </summary>
        public IEnumerable<IAppModule> Modules { get; private set; }

        public IServiceCollection LoadModules(IServiceCollection services)
        {
            var assemblies = AssemblyFinder.AllAssemblys;

            //所有 AppModule 派生类
            Type[] packTypes = assemblies.SelectMany(assembly => assembly.GetTypes())
               .Where(type => type.IsDeriveClassFrom<IAppModule>()).Distinct().ToArray();

            packTypes.Select(m => (IAppModule)Activator.CreateInstance(m));

            List<IAppModule> list = new List<IAppModule>();
            list.AddRange(packTypes.Select(m => (IAppModule)Activator.CreateInstance(m)));

            // 按先层级后顺序的规则进行排序
            list = list.OrderBy(m => m.Level).ThenBy(m => m.Order).ToList();
            Modules = list;
            foreach (IAppModule module in Modules)
            {
                services = module.AddServices(services);
            }

            return services;
        }

        /// <summary>
        /// 应用模块服务
        /// </summary>
        /// <param name="provider">服务提供者</param>
        public void UseModules(IApplicationBuilder provider)
        {
            ServiceLocator.Instance.SetServiceProvider(provider.ApplicationServices);

            ILogger logger = provider.ApplicationServices.GetLogger<AppModuleManager>();
            logger.LogInformation("框架初始化开始");
            DateTime dtStart = DateTime.Now;

            foreach (IAppModule module in Modules)
            {
                module.InitModule(provider);
                logger.LogInformation($"模块{module.GetType()}加载成功");
            }

            TimeSpan ts = DateTime.Now.Subtract(dtStart);
            logger.LogInformation($"框架初始化完成，耗时：{ts:g}");
        }
    }
}
