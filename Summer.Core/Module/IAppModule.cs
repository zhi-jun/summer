﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Summer.Core.Module
{
    /// <summary>
    /// 模块接口
    /// </summary>
    public interface IAppModule
    {
        /// <summary>
        /// 获取 模块级别，级别越小越先启动
        /// </summary>
        ModuleLevel Level { get; }

        /// <summary>
        /// 获取 模块启动顺序，模块启动的顺序先按级别启动，同一级别内部再按此顺序启动，
        /// 级别默认为0，表示无依赖，需要在同级别有依赖顺序的时候，再重写为>0的顺序值
        /// </summary>
        int Order { get; }

        /// <summary>
        /// 将模块服务添加到依赖注入服务容器中
        /// </summary>
        /// <param name="services">依赖注入服务容器</param>
        /// <returns></returns>
        IServiceCollection AddServices(IServiceCollection services);
        /// <summary>
        /// 初始化模块服务
        /// </summary>
        /// <param name="provider">服务提供者</param>
        void InitModule(IApplicationBuilder provider);
    }
}
