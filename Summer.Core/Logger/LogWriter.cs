﻿using Microsoft.Extensions.Logging;
using Summer.Core.Dependency;
using System;

namespace Summer.Core.Logger
{
    /// <summary>
    /// 日志
    /// </summary>
    public class LogWriter
    {
        #region ILogger
        //单例(延时+缓存)
        private static readonly Lazy<ILogger> InstanceLazy = new Lazy<ILogger>(() =>
        {
            ILoggerFactory factory = ServiceLocator.Instance.GetService<ILoggerFactory>();
            return factory.CreateLogger("");
        });

        //日志对象
        private static ILogger Logger => InstanceLazy.Value;
        #endregion

        #region Info
        public static void Info(string message)
        {
            if (string.IsNullOrEmpty(message))
                return;
            Logger.LogInformation("\r\n" + message);
        }
        #endregion

        #region Warn
        public static void Warn(string message)
        {
            if (string.IsNullOrEmpty(message))
                return;
            Logger.LogWarning("\r\n" + message);
        }
        #endregion

        #region Error
        public static void Error(string message, Exception ex = null)
        {
            if (string.IsNullOrEmpty(message) && ex == null)
                return;
            Logger.LogError(ex, message);
        }
        #endregion
    }
}
