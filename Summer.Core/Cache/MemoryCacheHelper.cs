﻿using System;
using Microsoft.Extensions.Caching.Memory;

namespace Summer.Core.Cache
{
    /// <summary>
    /// MemoryCache
    /// </summary>
    public class MemoryCacheHelper
    {
        private static readonly Lazy<MemoryCache> InstanceLazy = new Lazy<MemoryCache>(() => new MemoryCache(new MemoryCacheOptions()));
        private static MemoryCache Cache => InstanceLazy.Value;

        /// <summary>
        /// 验证缓存项是否存在
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));
            return Cache.TryGetValue(key, out _);
        }

        #region 获取缓存

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        public static T Get<T>(string key) where T : class
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));

            return Cache.Get(key) as T;
        }

        #endregion

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <param name="value">缓存Value</param>
        /// <param name="expiresIn">缓存时长</param>
        /// <returns></returns>
        public static bool Set(string key, object value, TimeSpan? expiresIn = null)
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (expiresIn == null)
                Cache.Set(key, value);
            else
                Cache.Set(key, value, new MemoryCacheEntryOptions().SetAbsoluteExpiration(expiresIn.Value));

            return Exists(key);
        }

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiresIn">缓存时长(分钟)</param>
        /// <returns></returns>
        public static bool Set(string key, object value, int expireInMinutes)
        {
            return Set(key, value, TimeSpan.FromMinutes(expireInMinutes));
        }

        #region 删除缓存

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key">缓存Key</param>
        /// <returns></returns>
        public static void Remove(string key)
        {
            if (key == null)
                throw new ArgumentNullException(nameof(key));

            Cache.Remove(key);
        }

        #endregion
    }
}
